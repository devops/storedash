SHELL = /bin/bash

build_tag ?= storedash

.PHONY: build
build:
	docker build -t $(build_tag) --build-arg ruby_version=$(shell cat .ruby-version) .

.PHONY: helm-update
helm-update:
	helm dependency update chart

.PHONY: helm-lint
helm-lint:
	helm lint chart --with-subcharts

.PHONY: chart
chart: helm-update helm-lint

.PHONY: audit
audit:
	docker run --rm -v "$(shell pwd):/app" $(build_tag) ./audit.sh

.PHONY: lock
lock:
	docker run --rm -v "$(shell pwd):/app" $(build_tag) /bin/bash -c '\
		git config --global --add safe.directory "*" && bundle lock'

.PHONY: update
update:
	docker run --rm -v "$(shell pwd):/app" $(build_tag) /bin/bash -c '\
		git config --global --add safe.directory "*" && bundle update'
