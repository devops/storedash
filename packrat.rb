require 'net/http'
require 'uri'
require_relative './packrat/volume.rb'

module Packrat

  TOKEN_EXCHANGE_URL = 'https://idms-web-ws.oit.duke.edu/idm-ws/clientSecret/createClientToken'

  PACKRAT_API_URL = 'https://packrat.oit.duke.edu/api/v2'

  def self.api_request(path)
    request = packrat_request(path)
    response = Net::HTTP.start(request.uri.hostname, request.uri.port, {use_ssl: true}) do |http|
      http.request(request)
    end
    response.value # raises Net::HTTPServerException
    JSON.parse(response.body)
  end

  def self.packrat_request(path)
    Net::HTTP::Get.new(packrat_uri(path)).tap do |request|
      request.content_type = "application/json"
      request["authorization"] = "Bearer #{cached_access_token}"
    end
  end

  def self.packrat_uri(path)
    URI.parse(PACKRAT_API_URL + path)
  end

  def self.get_volumes
    api_request("/volumes").map { |v| Volume.new(v) }
  end

  def self.get_volume(volume_id)
    Volume.new api_request("/volumes/#{volume_id}")
  end

  def self.token_exchange_payload
    { longLivedToken: ENV.fetch('PACKRAT_API_TOKEN'),
      clientId: 'packrat-production',
      endpoints: ['endpoint:/api/v2']
    }
  end

  def self.cached_access_token
    cache.fetch('packrat_api_token') do
      get_access_token
    end
  end

  def self.cache
    #
    # Tokens have a 10 minute expiry as of 2023-07-17.--DCS
    #
    @cache ||= ActiveSupport::Cache.lookup_store(:file_store, '/tmp/cache', {expires_in: 300})
  end

  def self.get_access_token
    uri = URI.parse(TOKEN_EXCHANGE_URL)
    request = Net::HTTP::Post.new(uri)
    request.content_type = "application/json"
    request.body = token_exchange_payload.to_json

    response = Net::HTTP.start(uri.hostname, uri.port, {use_ssl: true}) do |http|
      http.request(request)
    end

    response.value # raises Net::HTTPServerException

    payload = JSON.parse(response.body)
    access_token = payload.dig("mapQueryResult", "attributes", "accessToken")

    if access_token.nil?
      raise "Unable to get access token"
    end

    access_token
  end

end
