require 'json'
require 'sinatra'
require_relative './packrat.rb'

version = ENV.fetch('APP_VERSION', 'unknown')
$stdout.puts "StoreDash (#{version})"

get '/', provides: ['html'] do
  volumes = Packrat.get_volumes.sort_by { |v| v.name }
  erb :index, locals: {volumes: volumes}
end

get '/', provides: ['json'] do
  volumes = Packrat.get_volumes.map(&:as_json)
  JSON.dump(volumes)
end

get '/:id', provides: ['json'] do
  volume = Packrat.get_volume(params['id'])
  JSON.dump(volume.as_json)
end
