ARG ruby_version="3.2"

FROM ruby:${ruby_version}

ARG build_date="1970-01-01T00:00:00Z"
ARG app_version="0.0.0"

ENV APP_VERSION="${app_version}" \
    RACK_ENV="production" \
    HOME="/app"

RUN apt -y update && apt -y install jq && rm -rf /var/lib/apt/lists/*

LABEL org.opencontainers.artifact.title="DUL Storage Dashboard"
LABEL org.opencontainers.artifact.description="Duke University Libraries Storage Dashboard"
LABEL org.opencontainers.image.url="https://storedash.lib.duke.edu"
LABEL org.opencontainers.image.source="https://gitlab.oit.duke.edu/devops/storedash"
LABEL org.opencontainers.image.version="${app_version}"
LABEL org.opencontainers.image.authors="Duke University Libraries (https://library.duke.edu)"
LABEL org.opencontainers.image.vendor="Duke University Libraries"
LABEL org.opencontainers.image.licenses="BSD-3-Clause"
LABEL org.opencontainers.image.created="${build_date}"

WORKDIR /app

COPY . .

RUN gem install bundler -v "$(tail -1 Gemfile.lock | awk '{print $1}')" && \
    bundle install && \
    useradd -r -g 0 app && \
    chmod -R g=u . $GEM_HOME

EXPOSE 4567

CMD ["bundle", "exec", "ruby", "storedash.rb", "-o", "0.0.0.0"]

USER app
