require 'hashie'
require 'active_support'

module Packrat
  class Volume < Hashie::Mash

    GB = 1_073_741_824

    def free_gb
      max_gb - used_gb
    end

    def free_tb
      free_gb / 1024
    end

    def free_b
      free_gb * GB
    end

    def max_b
      max_gb * GB
    end

    def used_b
      used_gb * GB
    end

    def usage
      @usage ||= (used_gb * 100) / max_gb
    end

    def free
      ActiveSupport::NumberHelper.number_to_human_size(free_b)
    end

    def used
      ActiveSupport::NumberHelper.number_to_human_size(used_b)
    end

    def capacity
      ActiveSupport::NumberHelper.number_to_human_size(max_b)
    end

    def status
      case usage
      when 95..100
        'danger'
      when 90..94
        'warning'
      when 80..89
        'info'
      else
        'success'
      end
    end

    def as_json
      { id:, name:, used_gb:, max_gb:, free_gb:, free:, used:, capacity:, usage: }
    end
  end
end
